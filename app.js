require("./core/model/sl_object/repository").init();
require("./core/model/sl_room/repository").init();
require("./core/model/sl_event/repository").init();
require("./core/model/sl_object_property/repository").init();

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyparser = require('body-parser');


const app = express();
app.locals.helpersROOMS = function () {
    const AlertRepository = require("./core/model/sl_alert/respository");
    const list = AlertRepository.getAllAlertFromArray();
    console.log(list);
    return list;
};
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyparser.urlencoded({extended: true}));

app.use('/', require('./routes/index/index'));
app.use('/room', require('./routes/room/index'));
app.use('/object', require('./routes/object/index'));
app.use('/scenario', require('./routes/scenario/index'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
