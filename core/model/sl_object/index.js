const ObjectDatabase = require("./db");

class ObjectModel {
    constructor(args) {
        this._ID = (args !== undefined && args.ID !== undefined) ? parseInt(args.ID) : null;
        this._TITLE = (args !== undefined && args.TITLE) ? args.TITLE : null;
        this._DESCRIPTION = (args !== undefined && args.DESCRIPTION) ? args.DESCRIPTION : null;
    }

    get ID() {
        return this._ID;
    }

    set ID(value) {
        this._ID = parseInt(value);
    }

    get TITLE() {
        return this._TITLE;
    }

    set TITLE(value) {
        this._TITLE = value;
    }

    get DESCRIPTION() {
        return this._DESCRIPTION;
    }

    set DESCRIPTION(value) {
        this._DESCRIPTION = value;
    }

    save() {
        const Repository = require("./repository");
        return (async () => {
            await ObjectDatabase.saveObject(this);
            Repository.addObject(this);
        })();
    }

    remove() {
        const ObjectRepository = require("./repository");
        const ObjectPropertyRepository = require("../sl_object_property/repository");
        return (async () => {
            // Удаляем свойства объекта
            for (const value of ObjectPropertyRepository.getObjectPropertyFromObjectToArray(this)) {
                value.remove();
            }
            // Удаляем объект из репозитория
            ObjectRepository.removeObject(this);
            // Удаляем объект из БД
            ObjectDatabase.removeObject(this);
        })();
    }

    update() {
        return (async () => {
            ObjectDatabase.updateObject(this);
        })();
    }
}

module.exports = ObjectModel;