const ObjectModel = require("../");
const ObjectDatabase = require("../db");

class ObjectFactory {
    createObjectAndAddToRepository(Repository) {
        return (async () => {
            const ResultDB = await ObjectDatabase.getAllObject();
            ResultDB.result.forEach(value => {
                Repository.addObject(new ObjectModel(value));
            })
        })();
    }
}

module.exports = new ObjectFactory();