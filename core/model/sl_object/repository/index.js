const ObjectFactory = require("../factory");

class ObjectRepository {
    constructor() {
        this.repository = new Map();
    }

    init() {
        ObjectFactory.createObjectAndAddToRepository(this);
    }

    addObject(Object) {
        if (!this.repository.has(Object.ID)) {
            console.log(Object);
            this.repository.set(Object.ID, Object);
        }
    }

    removeObject(Object) {
        if (this.repository.has(Object.ID)) {
            this.repository.delete(Object.ID);
        }
    }

    getAllObjectToArray() {
        const list = [];
        for (const value of this.repository.values()) {
            list.push(value);
        }
        return list;
    }

    getObjectFromId(id) {
        id = parseInt(id);
        if (this.repository.has(id)) {
            return this.repository.get(id);
        }
    }
}

module.exports = new ObjectRepository();