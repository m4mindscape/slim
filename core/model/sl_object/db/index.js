const Database = require("../../../database");

class ObjectDatabase {
    constructor() {
        this.table = "sl_object";
    }

    getAllObject() {
        return (async () => {
            const sql = "SELECT * FROM " + this.table;
            return await Database.select(sql);
        })();
    }

    saveObject(Object) {
        return (async () => {
            const sql = "INSERT INTO " + this.table + " (`TITLE`, `DESCRIPTION`) VALUES ?";
            const resultDB = await Database.insert(sql, [[Object.TITLE, Object.DESCRIPTION]]);
            Object.ID = resultDB.result.insertId;
            return resultDB;
        })();
    }

    removeObject(Object) {
        return (async () => {
            const sql = "DELETE FROM " + this.table + " WHERE ID = " + Object.ID;
            const resultDB = await Database.delete(sql);
            return resultDB;
        })();
    }
    updateObject(Object) {
        return (async () => {
            const sql = "UPDATE "+this.table + " SET TITLE = ?, DESCRIPTION = ? WHERE ID = ?";
            const resultDB = await Database.delete(sql,[
                Object.TITLE,
                Object.DESCRIPTION,
                Object.ID
            ]);
            return resultDB;
        })();
    }
}

module.exports = new ObjectDatabase();