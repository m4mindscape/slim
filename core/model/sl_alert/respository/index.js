class AlertRepository {
    constructor() {
        this.repository = new Map();
    }

    addAlert(Alert) {
        const sizeRep = this.repository.size + 1;
        Alert.ID = sizeRep;
        this.repository.set(sizeRep, Alert);
    }

    getAllAlertFromArray() {
        const list = [];
        for (const value of this.repository.values()) {
            list.push(value);
        }
        return list;
    }
}

module.exports = new AlertRepository();