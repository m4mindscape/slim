class Alert {
    constructor() {
        this._ID = null;
        this._TITLE = null;
        this._TEXT = null;
        this._TYPE = null;
    }


    get ID() {
        return this._ID;
    }

    set ID(value) {
        this._ID = value;
    }

    get TITLE() {
        return this._TITLE;
    }

    set TITLE(value) {
        this._TITLE = value;
    }

    get TEXT() {
        return this._TEXT;
    }

    set TEXT(value) {
        this._TEXT = value;
    }

    get TYPE() {
        return this._TYPE;
    }

    set TYPE(value) {
        this._TYPE = value;
    }
}

module.exports = Alert;