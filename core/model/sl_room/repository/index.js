const Factory = require("../factory");

class RoomRepository {
    constructor() {
        this.repository = new Map();
    }

    init() {
        Factory.createRoomsAndAddToRepository(this);
    }

    addRoom(Room) {
        if (!this.repository.has(Room.ID)) {
            console.log(Room);
            this.repository.set(Room.ID, Room);
        }
    }

    deleteRoom(Room) {
        if (this.repository.has(Room.ID)) {
            this.repository.delete(Room.ID);
        }
    }

    getCountRoom() {
        return this.repository.size;
    }

    getAllRoomToArray() {
        const list = [];
        for (const value of this.repository.values()) {
            list.push(value);
        }
        return list;
    }

    getRoomFromId(id) {
        return this.repository.get(parseInt(id));
    }

    getRoomFromPropertyObject(PropertyObject) {
        for(const value of this.repository.values()) {
            if (value.ID_PROPERTY_OBJECT === PropertyObject.ID) {
                return value;
            }
        }
        return false;
    }
}

module.exports = new RoomRepository();