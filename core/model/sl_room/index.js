const Moment = require("moment");
const RoomDatabase = require("./db");

class RoomModel {
    constructor(args) {
        this._ID = (args !== undefined && args.ID !== undefined) ? parseInt(args.ID) : null;
        this._ID_PROPERTY_OBJECT = (args !== undefined && args.ID_PROPERTY_OBJECT !== undefined) ? parseInt(args.ID_PROPERTY_OBJECT) : null;
        this._TITLE = (args !== undefined && args.TITLE !== undefined) ? args.TITLE : null;
        this._DESCRIPTION = (args !== undefined && args.DESCRIPTION !== undefined) ? args.DESCRIPTION : null;
        this._PATH = (args !== undefined && args.PATH !== undefined) ? args.PATH : null;
        this._UPDATE_TIME = (args !== undefined && args.UPDATE_TIME !== undefined) ? Moment(args.UPDATE_TIME).format('YYYY-MM-DD HH:mm:ss') : Moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
        this._VALUE = (args !== undefined && args.VALUE !== undefined) ? args.VALUE : null;
    }


    get ID() {
        return this._ID;
    }

    set ID(value) {
        this._ID = parseInt(value);
    }

    get TITLE() {
        return this._TITLE;
    }

    set TITLE(value) {
        this._TITLE = value;
    }

    get DESCRIPTION() {
        return this._DESCRIPTION;
    }

    set DESCRIPTION(value) {
        this._DESCRIPTION = value;
    }

    get PATH() {
        return this._PATH;
    }

    set PATH(value) {
        this._PATH = value;
    }

    get UPDATE_TIME() {
        return this._UPDATE_TIME;
    }

    get VALUE() {
        return this._VALUE;
    }

    set VALUE(value) {
        this._VALUE = value;
        // Если есть привязка к свойству объекта, то обновляем его значение
        if (this.ID_PROPERTY_OBJECT > 0) {
            const ObjectPropertyRepository = require("../sl_object_property/repository");
            const Property = ObjectPropertyRepository.getObjectPropertyFromId(this.ID_PROPERTY_OBJECT);
            // Сравниваем значения если они не совпадают, то обновляем их
            // И заносиим изменения в БД
            if (Property.VALUE !== this.VALUE) {
                Property.VALUE = this.VALUE;
            }
        }
    }


    get ID_PROPERTY_OBJECT() {
        return this._ID_PROPERTY_OBJECT;
    }

    set ID_PROPERTY_OBJECT(value) {
        this._ID_PROPERTY_OBJECT = parseInt(value);
    }

    /* Методы */
    save() {
        const RoomRepository = require("./repository");
        return (async () => {
            await RoomDatabase.saveRoom(this);
            RoomRepository.addRoom(this);
        })();
    }

    update() {
        this._UPDATE_TIME = Moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
        RoomDatabase.updateRoom(this);
    }

    delete() {
        const RoomRepository = require("./repository");
        RoomDatabase.deleteRoom(this);
        RoomRepository.deleteRoom(this);
    }
}

module.exports = RoomModel;
