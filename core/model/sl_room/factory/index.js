const Database = require("../db");
const Room = require("../");

class RoomFactory {
    createRoomsAndAddToRepository(Repository) {
        return (async () => {
            const rooms = await Database.getAllRoom();
            for (const key in rooms.result) {
                Repository.addRoom(new Room(rooms.result[key]));
            }
        })();
    }
}

module.exports = new RoomFactory();