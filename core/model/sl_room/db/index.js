const Database = require("../../../database");

class RoomDatabase {
    constructor() {
        this.table = "sl_tpsl_room";
    }

    getAllRoom() {
        return (async () => {
            const sql = "SELECT * FROM " + this.table;
            return await Database.select(sql);
        })();
    }

    saveRoom(Room) {
        return (async () => {
            const sql = "INSERT INTO " + this.table + " (`TITLE`, `PATH`, `UPDATE_TIME`, `VALUE`, `ID_PROPERTY_OBJECT`, `DESCRIPTION`) VALUES ?";
            const result = await Database.insert(sql, [[
                Room.TITLE,
                Room.PATH,
                Room.UPDATE_TIME,
                Room.VALUE,
                Room.ID_PROPERTY_OBJECT,
                Room.DESCRIPTION]]);
            Room.ID = result.result.insertId;
        })();
    }

    updateRoom(Room) {
        return (async () => {
            const sql = "UPDATE " + this.table + " SET ID_PROPERTY_OBJECT = ?, TITLE = ?, PATH = ?, UPDATE_TIME = ?, VALUE = ?, DESCRIPTION = ? WHERE  ID = ?";
            return await Database.update(sql, [Room.ID_PROPERTY_OBJECT, Room.TITLE, Room.PATH, Room.UPDATE_TIME, Room.VALUE, Room.DESCRIPTION, Room.ID]);
        })();
    }

    deleteRoom(Room) {
        return (async () => {
            const sql = "DELETE FROM " + this.table + " WHERE ID = ?";
            Database.delete(sql, [Room.ID]);
        })();
    }
}

module.exports = new RoomDatabase();