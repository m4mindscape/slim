const Database = require("../../../database");

class EventDatabase {
    constructor() {
        this.table = "sl_event";
    }

    getAllEvent() {
        return (async () => {
            const sql = "SELECT * FROM " + this.table;
            return await Database.select(sql);
        })();
    }

    saveEvent(Event) {
        return (async () => {
            const sql = "INSERT INTO " + this.table + " (`TITLE`, `DESCRIPTION`, `CODE`) VALUES ?";
            const resultDB = await Database.insert(sql, [[
                Event.TITLE,
                Event.DESCRIPTION,
                Event.CODE
            ]]);
            Event.ID = resultDB.result.insertId;
            return resultDB;
        })();
    }

    removeEvent(Event) {
        return (async () => {
            const sql = "DELETE FROM " + this.table + " WHERE ID = " + Event.ID;
            return await Database.delete(sql);
        })();
    }

    updateEvent(Event) {
        return (async () => {
            const sql = "UPDATE " + this.table + " SET TITLE = ?, DESCRIPTION = ?, CODE = ? WHERE ID = ?";
            return await Database.select(sql, [
                Event.TITLE,
                Event.DESCRIPTION,
                Event.CODE,
                Event.ID
            ])
        })();
    }
}

module.exports = new EventDatabase();