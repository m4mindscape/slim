const EventDatabase = require("./db");
const ModuleEvent = require("../../module/event/run");

class EventModel {
    constructor(args) {
        this._ID = (args !== undefined && args.ID !== undefined) ? args.ID : null;
        this._TITLE = (args !== undefined && args.TITLE !== undefined) ? args.TITLE : null;
        this._DESCRIPTION = (args !== undefined && args.DESCRIPTION !== undefined) ? args.DESCRIPTION : null;
        this._CODE = (args !== undefined && args.CODE !== undefined) ? args.CODE : null
    }


    get ID() {
        return this._ID;
    }

    set ID(value) {
        this._ID = value;
    }

    get TITLE() {
        return this._TITLE;
    }

    set TITLE(value) {
        this._TITLE = value;
    }

    get DESCRIPTION() {
        return this._DESCRIPTION;
    }

    set DESCRIPTION(value) {
        this._DESCRIPTION = value;
    }

    get CODE() {
        return this._CODE;
    }

    set CODE(value) {
        this._CODE = value;
    }

    save() {
        const EventRepository = require("./repository");
        return (async () => {
            await EventDatabase.saveEvent(this);
            EventRepository.addEvent(this);
        })();
    }

    remove() {
        const EventRepository = require("./repository");
        return (async () => {
            EventRepository.removeEvent(this);
            await EventDatabase.removeEvent(this);
        })();
    }

    update() {
        return (async () => {
            return await EventDatabase.updateEvent(this);
        })();
    }

    runEvent() {
        new ModuleEvent(this).init();
    }
}

module.exports = EventModel;