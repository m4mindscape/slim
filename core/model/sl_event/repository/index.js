const EventFactory = require("../factory");

class EventRepository {
    constructor() {
        this.repository = new Map();
    }

    init() {
        EventFactory.createEventAndAddToRepository(this);
    }

    addEvent(Event) {
        if (!this.repository.has(Event.ID)) {
            console.log(Event);
            this.repository.set(Event.ID, Event);
        }
    }

    removeEvent(Event) {
        if (this.repository.has(Event.ID)) {
            this.repository.delete(Event.ID);
        }
    }

    getAllEventToArray() {
        const list = [];
        for (const value of this.repository.values()) {
            list.push(value);
        }
        return list;
    }

    getEventFromId(id) {
        return this.repository.get(parseInt(id));
    }
}

module.exports = new EventRepository();