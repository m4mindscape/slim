const EventDatabase = require("../db");
const Event = require("../");
class EventFactory {
    constructor() {

    }
    createEventAndAddToRepository (Repository) {
        (async () => {
            const resultDB = await EventDatabase.getAllEvent();
            resultDB.result.forEach((value) => {
                Repository.addEvent(new Event(value));
            })
        })();
    }
}

module.exports = new EventFactory();