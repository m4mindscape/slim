const ObjectPropertyFactory = require("../factory");

class ObjectPropertyRepository {
    constructor() {
        this.repository = new Map();
    }

    init() {
        ObjectPropertyFactory.createAllObjectPropertyAndAddToRepository(this);
    }

    addObjectProperty(ObjectProperty) {
        if (!this.repository.has(ObjectProperty.ID)) {
            console.log(ObjectProperty);
            this.repository.set(ObjectProperty.ID, ObjectProperty);
        }
    }

    removeObjectProperty(ObjectProperty) {
        if (this.repository.has(ObjectProperty.ID)) {
            this.repository.delete(ObjectProperty.ID);
        }
    }

    getAllObjectPropertyToArray() {
        const list = [];
        for (const value of this.repository.values()) {
            list.push(value);
        }
        return list;
    }

    getObjectPropertyFromObjectToArray(Object) {
        const list = [];
        for (const value of this.repository.values()) {
            if (value.ID_OBJECT === Object.ID) {
                list.push(value);
            }
        }
        return list;
    }

    getObjectPropertyFromId(id) {
        id = parseInt(id);
        return this.repository.get(id);
    }
}

module.exports = new ObjectPropertyRepository();