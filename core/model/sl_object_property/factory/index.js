const ObjectPropertyDatabase = require("../db");
const ObjectPropertyModel = require("../");

class ObjectPropertyFactory {
    createAllObjectPropertyAndAddToRepository(Repository) {
        (async () => {
            const resultDB = await ObjectPropertyDatabase.getAllObjectProperty();
            resultDB.result.forEach(value => {
                Repository.addObjectProperty(new ObjectPropertyModel(value));
            })
        })();
    }
}

module.exports = new ObjectPropertyFactory();