const Database = require("../../../database");

class ObjectProprtyDatabase {
    constructor() {
        this.table = "sl_object_property";
    }

    getAllObjectProperty() {
        return (async () => {
            const sql = "SELECT * FROM " + this.table;
            return await Database.select(sql);
        })();
    }

    savePropertyObject(ObjectProperty) {
        return (async () => {
            const sql = "INSERT INTO " + this.table + " (`ID_OBJECT`, `ID_EVENT`, `TITLE`, `VALUE`) VALUES ?";
            const resultDB = await Database.insert(sql, [[
                ObjectProperty.ID_OBJECT,
                ObjectProperty.ID_EVENT,
                ObjectProperty.TITLE,
                ObjectProperty.VALUE
            ]]);
            ObjectProperty.ID = resultDB.result.insertId;
        })();
    }

    removePropertyObject(ObjectProperty) {
        return (async () => {
            const sql = "DELETE FROM " + this.table + " WHERE ID = " + ObjectProperty.ID;
            return await Database.delete(sql);
        })();
    }

    updatePropertyObject(ObjectProperty) {
        return (async () => {
            const sql = "UPDATE " + this.table + " SET TITLE = ?, ID_EVENT = ?, TITLE = ?, VALUE = ?, UPDATE_ROOM_VALUE = ? WHERE ID = ?";
            return await Database.update(sql, [
                ObjectProperty.TITLE,
                ObjectProperty.ID_EVENT,
                ObjectProperty.TITLE,
                ObjectProperty.VALUE,
                ObjectProperty.UPDATE_ROOM_VALUE,
                ObjectProperty.ID
            ])
        })();
    }
}

module.exports = new ObjectProprtyDatabase();