const ObjectPropertyDatabase = require("./db");


class ObjectProperty {
    constructor(args) {
        this._ID = (args !== undefined && args.ID !== undefined) ? parseInt(args.ID) : null;
        this._ID_OBJECT = (args !== undefined && args.ID_OBJECT !== undefined) ? parseInt(args.ID_OBJECT) : null;
        this._ID_EVENT = (args !== undefined && args.ID_EVENT !== undefined) ? parseInt(args.ID_EVENT) : null;
        this._TITLE = (args !== undefined && args.TITLE !== undefined) ? args.TITLE : null;
        this._VALUE = (args !== undefined && args.VALUE !== undefined) ? args.VALUE : null;
        this._UPDATE_ROOM_VALUE = (args !== undefined && args.UPDATE_ROOM_VALUE !== undefined) ? parseInt(args.UPDATE_ROOM_VALUE) : 0;
    }


    get ID() {
        return this._ID;
    }

    set ID(value) {
        this._ID = parseInt(value);
    }

    get ID_OBJECT() {
        return this._ID_OBJECT;
    }

    set ID_OBJECT(value) {
        this._ID_OBJECT = parseInt(value);
    }

    get ID_EVENT() {
        return this._ID_EVENT;
    }

    set ID_EVENT(value) {
        this._ID_EVENT = parseInt(value);
    }

    get TITLE() {
        return this._TITLE;
    }

    set TITLE(value) {
        this._TITLE = value;
    }

    get VALUE() {
        return this._VALUE;
    }

    set VALUE(value) {
        this._VALUE = value;
        // Если установлен параметр обновления значения привязаной комнаты
        if (this.UPDATE_ROOM_VALUE) {
            // Получаем комнату к которой привзянно свойство
            const RoomRepository = require("../sl_room/repository");
            const Room = RoomRepository.getRoomFromPropertyObject(this);
            // Если комната нашлась
            if (Room !== false) {
                // Если значения не совпадают, то обновляем значение комнаты
                // И заносим изменения в БД
                if (Room.VALUE !== this.VALUE) {
                    Room.VALUE = this.VALUE;
                    Room.update();
                }
            }
        }
        // Если есть привязка к событию
        if (this.ID_EVENT > 0) {
            const EventRepository = require("../sl_event/repository");
            const event = EventRepository.getEventFromId(this.ID_EVENT);
            event.runEvent();
        }
    }


    get UPDATE_ROOM_VALUE() {
        return this._UPDATE_ROOM_VALUE;
    }

    set UPDATE_ROOM_VALUE(value) {
        this._UPDATE_ROOM_VALUE = parseInt(value);
    }

    save() {
        const ObjectPropertyRepository = require("./repository");
        return (async () => {
            await ObjectPropertyDatabase.savePropertyObject(this);
            ObjectPropertyRepository.addObjectProperty(this);
        })();
    }

    remove() {
        const ObjectPropertyRepository = require("./repository");
        return (async () => {
            ObjectPropertyRepository.removeObjectProperty(this);
            return await ObjectPropertyDatabase.removePropertyObject(this);
        })();
    }

    update() {
        return (async () => {
            return await ObjectPropertyDatabase.updatePropertyObject(this);
        })();
    }
}

module.exports = ObjectProperty;