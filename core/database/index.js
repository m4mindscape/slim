const mysql = require("mysql");
const config = require("../config/config");

class Database {
    constructor() {
        this.connection = mysql.createPool({
            host    : config.database.host,
            user    : config.database.user,
            password: config.database.password,
            database: config.database.database
        });
    }

    select(sql, array) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, array, (err, result, fields) => {
                if (err) reject(err);
                resolve({result: result, fields: fields});
            })
        })
    }

    update(sql, array) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, array, (err, result, fields) => {
                if (err) reject(err);
                resolve({result: result, fields: fields});
            })
        });
    }

    insert(sql, array) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, [array], (err, result, fields) => {
                if (err) reject(err);
                resolve({result: result, fields: fields})
            });
        })
    }

    delete(sql, array) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, array, (err, result, fields) => {
                if (err) reject(err);
                resolve({result: result, fields: fields});
            })
        })
    }
}

module.exports = new Database();