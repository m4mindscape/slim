const util = require("util");
const ObjectRepository = require("../../../model/sl_object/repository");
const PropertyObjectRepository = require("../../../model/sl_object_property/repository");
const EventRepository = require("../../../model/sl_event/repository");
const RoomRepository = require("../../../model/sl_room/repository");
const AlertRepository = require("../../../model/sl_alert/respository");
const Alert = require("../../../model/sl_alert");

class API {
    getPropertyObjectFromID(id) {
        id = parseInt(id);
    }

    getObjectFromID() {
        id = parseInt(id);
    }

    runScenarioFromId(id) {
        id = parseInt(id);
        const event = RoomRepository.getRoomFromId(id);
        if (event !== undefined) {
            event.runEvent();
            return true;
        }
        return false;
    }

    setValuePropertyObjectFromID(id, value) {
        const property = PropertyObjectRepository.getObjectPropertyFromId(id);
        if (property !== undefined) {
            property.VALUE = value;
            property.update();
            return true;
        }
        return false;
    }

    createAlert(title, text, type) {
        const alert = new Alert();
        alert.TITLE = title;
        alert.TEXT = text;
        alert.TYPE = type;
        AlertRepository.addAlert(alert);
    }
}

module.exports = new API();
module.exports.methods = API;