const vm = require("vm");
const util = require("util");
const API = require("../api");

class RunCode {
    constructor(Event) {
        this.Event = Event;
        this.logs = [];
        this.context = {
            print: (args) => {
                const object = util.inspect(args, {showHidden: true, depth: null});
                this.logs.push(object);
            },
            API  : API,
            Methods : API.methods
        };
    }

    init() {
        return (async () => {
            try {
                new vm.runInNewContext(this.Event.CODE, this.context);
            }
            catch (e) {
                this.context.print(e.toString());
            }
        })();
    }
}

module.exports = RunCode;