const express = require('express');
const router = express.Router();
const Object = require("../../core/model/sl_object");
const ObjectProperty = require("../../core/model/sl_object_property");
const ObjectRepository = require("../../core/model/sl_object/repository");
const ObjectPropertyRepository = require("../../core/model/sl_object_property/repository");
const EventRepositry = require("../../core/model/sl_event/repository");

/* GET home page. */
router
    .get('/', (req, res, next) => {
        res.render('object/index', {
            title  : 'Объекты',
            objects: ObjectRepository.getAllObjectToArray(),
        });
    })
    .post("/add_object", async (req, res) => {
        const object = new Object(req.body);
        await object.save();
        res.redirect(303, "/object")
    })
    .get("/edit/:ID", (req, res) => {
        const object = ObjectRepository.getObjectFromId(req.params.ID);
        res.render('object/edit', {
            title         : "Редактировать",
            object        : object,
            objectProperty: ObjectPropertyRepository.getObjectPropertyFromObjectToArray(object),
            events        : EventRepositry.getAllEventToArray()
        });
    })
    .post("/object_update", (req, res) => {
        const object = ObjectRepository.getObjectFromId(req.body.ID);
        object.TITLE = req.body.TITLE;
        object.DESCRIPTION = req.body.DESCRIPTION;
        object.update();
        res.redirect(303, req.headers.referer)
    })
    .get("/remove_object/:ID", (req, res) => {
        const object = ObjectRepository.getObjectFromId(req.params.ID);
        object.remove();
        res.redirect(303, "/object");
    })
    .get("/remove_property/:ID", (req, res) => {
        const ObjectProperty = ObjectPropertyRepository.getObjectPropertyFromId(req.params.ID);
        ObjectProperty.remove();
        res.redirect(303, req.headers.referer);
    })
    .post("/add_property", async (req, res) => {
        await new ObjectProperty(req.body).save();
        res.redirect(303, "/object/edit/" + req.body.ID_OBJECT)
    })
    .get("/edit_property/:ID", (req, res) => {
        res.render('object/edit_property', {
            title   : "Редактировать свойство",
            property: ObjectPropertyRepository.getObjectPropertyFromId(req.params.ID),
            events  : EventRepositry.getAllEventToArray()
        });
    })
    .post("/update_property/", (req, res) => {
        const Property = ObjectPropertyRepository.getObjectPropertyFromId(req.body.ID);
        console.log(Property);
        Property.TITLE = req.body.TITLE;
        Property.VALUE = req.body.VALUE;
        Property.UPDATE_ROOM_VALUE = req.body.UPDATE_ROOM_VALUE;
        Property.ID_EVENT = req.body.ID_EVENT;
        Property.update();
        res.redirect(303, req.headers.referer);
    });


module.exports = router;
