const express = require('express');
const router = express.Router();
const EventRepository = require("../../core/model/sl_event/repository");
const Event = require("../../core/model/sl_event");
const ModuleRunEvent = require("../../core/module/event/run");
/* GET home page. */
router
    .get('/', (req, res, next) => {
        res.render('scenario/index', {
            title: 'Пользовательские сценарии',
            event: EventRepository.getAllEventToArray()
        });
    })
    .post('/add', async (req, res) => {
        await new Event(req.body).save();
        res.redirect(303, req.headers.referer);
    })
    .get("/remove/:ID", (req, res) => {
        EventRepository.getEventFromId(req.params.ID).remove();
        res.redirect(303, req.headers.referer);
    })
    .get("/edit/:ID", (req, res) => {
        const event = EventRepository.getEventFromId(req.params.ID);
        res.render('scenario/edit', {
            title: 'Пользовательские сценарии',
            "event": event
        });
    })
    .post("/update", (req, res) => {
        const event = EventRepository.getEventFromId(req.body.ID);
        event.TITLE = req.body.TITLE;
        event.DESCRIPTION = req.body.DESCRIPTION;
        event.CODE = req.body.CODE;
        event.update();
        res.redirect(303, req.headers.referer);
    })
    .get("/run_code/:ID", async (req, res) => {
        const event = new ModuleRunEvent(EventRepository.getEventFromId(req.params.ID));
        await event.init();
        res.render('scenario/run', {
            "title": 'Пользовательские сценарии',
            "event": event
        });
    });


module.exports = router;
