const express = require('express');
const router = express.Router();
const RoomModel = require("../../core/model/sl_room");
const RoomRepository = require("../../core/model/sl_room/repository");
const ObjectPropertyRepository = require("../../core/model/sl_object_property/repository");

/* GET home page. */
router
    .get('/', function (req, res, next) {
        res.render('room/index', {
            "title"   : 'Комнаты',
            "rooms"   : RoomRepository.getAllRoomToArray(),
            "property": ObjectPropertyRepository.getAllObjectPropertyToArray()
        });
    })
    .post('/add_room', async (req, res) => {
        const room = new RoomModel(req.body);
        await room.save();
        res.redirect(303, "/room")
    })
    .get("/delete/:ID", (req, res) => {
        const room = RoomRepository.getRoomFromId(req.params.ID);
        room.delete();
        res.redirect("/room/");
    })
    .get("/edit/:ID", (req, res) => {
        res.render('room/edit', {
            title   : 'Редактировать',
            room    : RoomRepository.getRoomFromId(req.params.ID),
            property: ObjectPropertyRepository.getAllObjectPropertyToArray()
        });
    })
    .post("/edit/update", (req, res) => {
        const Room = RoomRepository.getRoomFromId(req.body.ID);
        Room.TITLE = req.body.TITLE;
        Room.PATH = req.body.PATH;
        Room.DESCRIPTION = req.body.DESCRIPTION;
        Room.VALUE = req.body.VALUE;
        Room.ID_PROPERTY_OBJECT = req.body.ID_PROPERTY_OBJECT;
        console.log(Room);
        Room.update();
        res.redirect("/room/edit/" + req.body.ID);
    });

module.exports = router;
