const express = require('express');
const router = express.Router();
const RoomRepository = require("../../core/model/sl_room/repository");
const ObjectRepository = require("../../core/model/sl_object/repository");
const EventRepository = require("../../core/model/sl_event/repository");

/* GET home page. */
router
    .get('/', (req, res, next) => {
        res.render('index/index', {
            title    : 'Главная страница',
            rooms    : RoomRepository.getAllRoomToArray(),
            objects  : ObjectRepository.getAllObjectToArray(),
            events   : EventRepository.getAllEventToArray(),
        });
    });


module.exports = router;
